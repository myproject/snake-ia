import pygame
import time
from Board import Board
from Player import Player

# IA OR PLAYER
TYPEPLAY = "IA"

WINDOWSIZE = (500, 500)
BOARDSIZE = (20, 20)
CELLSIZE = (WINDOWSIZE[0] / BOARDSIZE[0], WINDOWSIZE[1] / BOARDSIZE[1])
EMPTYCOLOR = (0, 0, 0)
ENDCOLOR = (0, 255, 0)
PLAYERCOLOR = (255, 255, 255)

FPS = 60 if TYPEPLAY == "IA" else 10


def draw(fenetre, board, player):
    fenetre.fill((0, 0, 0))

    cases = board.getCases()

    for x in range(len(cases)):
        for y in range(len(cases[x])):
            case = cases[x][y]

            if case == "empty":
                color = EMPTYCOLOR
            else:
                color = ENDCOLOR

            pygame.draw.rect(fenetre, color, (x * CELLSIZE[0], y * CELLSIZE[1], CELLSIZE[0], CELLSIZE[1]))

    for pos in player.getBody():
        pygame.draw.rect(fenetre, PLAYERCOLOR, (pos[0] * CELLSIZE[0], pos[1] * CELLSIZE[1], CELLSIZE[0], CELLSIZE[1]))

    pygame.display.update()


def update(board, player):
    eatApple = board.update(player)

    if eatApple:
        player.addBody()
        board.moveEndCase()
    else:
        player.move()

    isEnd = False

    if player.eatHimSelf() or player.isOutside(BOARDSIZE):
        isEnd = True

    return isEnd


def main():
    pygame.init()
    fenetre = pygame.display.set_mode(WINDOWSIZE)
    pygame.display.set_caption("test IA")

    time_per_frame = 1 / FPS
    last_time = time.time()
    frameCount = 0

    board = Board(BOARDSIZE)
    player = Player()

    draw(fenetre, board, player)

    run = True
    while run:
        delta_time = time.time() - last_time

        if TYPEPLAY == "IA":
            player.iaDirection(board, BOARDSIZE)

        run = not update(board, player)
        draw(fenetre, board, player)

        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():
                keys = pygame.key.get_pressed()
                if event.type == pygame.QUIT or keys[pygame.K_ESCAPE]:
                    run = False

                if TYPEPLAY == "PLAYER":
                    for key in player.getPossibleDirection():
                        if keys[key]:
                            player.changeDirection(key)

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    main()
