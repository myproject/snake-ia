import random as r


class Board:
    def __init__(self, size):
        self.__cases = [["empty" for y in range(size[1])] for x in range(size[0])]
        self.setRandomEnd()

    def setRandomEnd(self):
        x = r.randint(0, len(self.__cases) - 1)
        y = r.randint(0, len(self.__cases[x]) - 1)

        self.__cases[x][y] = "end"

    def getCases(self):
        return self.__cases

    def getEndCase(self):
        pos = (0, 0)
        for x in range(len(self.__cases)):
            for y in range(len(self.__cases)):
                case = self.__cases[x][y]

                if case == "end":
                    pos = (x, y)

        return pos

    def update(self, player):
        pos = player.getHead()
        endPos = self.getEndCase()
        eatApple = False

        if pos[0] == endPos[0] and pos[1] == endPos[1]:
            eatApple = True

        return eatApple

    def moveEndCase(self):
        endPos = self.getEndCase()
        self.__cases[endPos[0]][endPos[1]] = "empty"
        self.setRandomEnd()
