import math
import pygame


class Player:
    def __init__(self):
        self.__direction = pygame.K_d
        self.__body = [[0, 0]]

    def getHead(self):
        return self.__body[0]

    def __getPositionByMove(self, direction = None):
        if direction is None:
            direction = self.__direction

        pos = list(self.__body[0])
        if direction == pygame.K_z:
            pos[1] = self.__body[0][1] - 1
        elif direction == pygame.K_s:
            pos[1] = self.__body[0][1] + 1
        elif direction == pygame.K_q:
            pos[0] = self.__body[0][0] - 1
        elif direction == pygame.K_d:
            pos[0] = self.__body[0][0] + 1

        return pos

    def move(self):
        for i in range(len(self.__body) - 1, 0, -1):
            self.__body[i] = self.__body[i - 1]

        self.__body[0] = self.__getPositionByMove()

    def addBody(self):
        self.__body.insert(0, self.__getPositionByMove())

    def changeDirection(self, direction):
        self.__direction = direction

    def getBody(self):
        return self.__body

    def eatHimSelf(self, pos = None):
        if pos is None:
            pos = self.__body[0]

        areEatingHimself = False

        for i in range(len(self.__body) - 1, 0, -1):
            if pos[0] == self.__body[i][0] and pos[1] == self.__body[i][1]:
                areEatingHimself = True

        return areEatingHimself

    def isOutside(self, boardSize, pos = None):
        if pos is None:
            pos = self.__body[0]

        areOutside = False
        if pos[0] < 0 or pos[0] == boardSize[0] or pos[1] < 0 or pos[1] == boardSize[1]:
            areOutside = True

        return areOutside

    def getPossibleDirection(self):
        all = [pygame.K_d, pygame.K_s, pygame.K_z, pygame.K_q]

        if self.__direction == pygame.K_d:
            all.remove(pygame.K_q)
        elif self.__direction == pygame.K_q:
            all.remove(pygame.K_d)
        elif self.__direction == pygame.K_z:
            all.remove(pygame.K_s)
        elif self.__direction == pygame.K_s:
            all.remove(pygame.K_z)

        return all

    def iaDirection(self, board, boardSize):
        res = None
        endPos = board.getEndCase()

        possibleMove = {pygame.K_z: 0, pygame.K_s: 0, pygame.K_q: 0, pygame.K_d: 0}
        resultMove = dict(possibleMove)
        nbr = 4
        for move, point in possibleMove.items():
            newPos = self.__getPositionByMove(move)

            if self.isOutside(boardSize, newPos) or self.eatHimSelf(newPos):
                del resultMove[move]
                nbr -= 1

            else:
                dist = math.dist(endPos, newPos)
                resultMove[move] = dist

        if nbr >= 1:
            res = min(resultMove, key=resultMove.get)

        if res is not None:
            self.__direction = res